Building a retirement calculator that actually takes fees, inflation, and social security decline into account. It even calculates the inflation rate correctly! (Very few do sadly.)

I was originally writing this only for myself, so the code is a horrible mess. My next task will be to make the code actually maintainable.
