<?php
//if(($_SERVER["HTTPS"] != "on") && (!headers_sent()))
//{
//    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
//    exit();
//}
//
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Calculate Your Retirement!</title>
<link rel="stylesheet" type="text/css" href="./style.css">
</head>
<body>
<?php
function getPage() {
//    if(isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on"){
//        return 'https://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//    }else{
        return 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//    }
}

// matches SEC calculator: https://www.investor.gov/additional-resources/free-financial-planning-tools/compound-interest-calculator
// Balance(Y)   =   P(1 + r)^Y   +   c[((1 + r)^Y - 1) / r]
function interest($investment,$contrib,$year,$in_rate=7){
    if($in_rate > 0){
        $rate = $in_rate / 100;
        $full_rate = pow($rate + 1, $year);
        $yearly_contrib = $contrib * 12;
        $total = $investment * $full_rate;

        $contrib_rate = (pow($rate + 1, $year) - 1) / $rate;
        $contrib_int = $yearly_contrib * $contrib_rate;
        $total += $contrib_int;
    
        return (double)$total;
    }else{
        return (double)($investment + ($contrib * 12 * $year));
    }
}

// matches: http://financeformulas.net/Present_Value.html
// present_value = future_value * ( 1 / (( 1 + r ) ^ Y) )
function adjust_for_inflation($amount, $years, $inflation_rate=2.5){
    $int_rate = 1 + ($inflation_rate / 100);
    $rate = pow($int_rate , $years);
    $inverse_rate = 1/$rate;
    return (double)$inverse_rate * $amount;
}

function get_fees($investment,$contrib,$years_before, $years_after, $in_rate_before, $in_rate_after, $fee_percent, $fee_dollar){  
    // Totals After Interest Before Retirement, without and with fees:
    $before_retirement_without_fees = interest($investment, $contrib, $years_before, $in_rate_before);
    $before_retirement_with_fees = interest($investment, $contrib, $years_before, ($in_rate_before - $fee_percent));
    
    // Totals After Interest After Retirement, without and with fees:
    $after_retirement_without_fees = get_annuity($before_retirement_with_fees, $years_after, $in_rate_after)*$years_after;
    $after_retirement_with_fees = get_annuity($before_retirement_with_fees, $years_after, ($in_rate_after - $fee_percent))*$years_after;
    
    // Total Flat Dollar Fees:
    $total_flat_fee_dollar = ($years_before + $years_after) * $fee_dollar;
    
    // Total Fees = (difference in without & with fees) + flat dollar fees;
    $total_fees = ($before_retirement_without_fees - $before_retirement_with_fees) + ($after_retirement_without_fees - $after_retirement_with_fees) + $total_flat_fee_dollar;
    
    return (double)$total_fees;
}

/*
2018 Tax Rates:
Rate            Unmarried               Married
10%             $0 - $9,525              $0 - $19,050
12%             $9,526 - $38,700         $19,051 - $77,400
22%             $38,701 - $82,500        $77,401 - $165,000
24%             $82,501 - $157,500       $165,001 - $315,000
32%             $157,501 - $200,000      $315,001 - $400,000
35%             $200,001 - $500,000      $400,001 - $600,000
37%             $500,001 - up            $600,001 - up
*/
function get_annual_taxes($yearly_distribution, $married = true){
    $tax_due = 0;
    
    if($married == true){
        if($yearly_distribution <= 19050){
             // 10%
            $tax_due = $yearly_distribution * 0.1;
        }else{
             // 10% of first $19,050 
            $tax_due = 1905;
            $yearly_distribution -= 19050;
            if ($yearly_distribution > 0){
                if ($yearly_distribution <= 58350) {
                    $tax_due = $tax_due + ($yearly_distribution * 0.12); 
                }else{
                     // 12% of next $58,350 ($77,400 - $19,050)
                    $tax_due = $tax_due + 7002;
                    $yearly_distribution -= 58350;
                    if ($yearly_distribution > 0){
                        if ($yearly_distribution <= 87600) {
                            $tax_due = $tax_due + ($yearly_distribution * 0.22);
                        }else{
                             // 22% of next $87,600 ($165,000 - $58,350 - $19,050)
                            $tax_due = $tax_due + 19272;
                            $yearly_distribution -= 87600;
                            if ($yearly_distribution > 0){
                                if ($yearly_distribution <= 150000) {
                                    $tax_due = $tax_due + ($yearly_distribution * 0.24);
                                }else{
                                     // 24% of next $150,000 ($315,000 - $87,600 - $58,350 - $19,050)
                                    $tax_due = $tax_due + 36000;
                                    $yearly_distribution -= 150000;
                                    if ($yearly_distribution > 0){
                                        if ($yearly_distribution <= 85000) {
                                            $tax_due = $tax_due + ($yearly_distribution * 0.32);
                                        }else{
                                             // 32% of next $85,000 ($400,000 - $150,000 - $87,600 - $58,350 - $19,050)
                                            $tax_due = $tax_due + 27200;
                                            $yearly_distribution -= 85000;
                                            if ($yearly_distribution > 0){
                                                if ($yearly_distribution <= 200000) {
                                                    $tax_due = $tax_due + ($yearly_distribution * 0.35);
                                                }else{
                                                     // 35% of next $200,000 ($600,000 - $85,000 - $150,000 - $87,600 - $58,350 -   $19,050)
                                                    $tax_due = $tax_due + 70000;
                                                    $yearly_distribution -= 200000;
                                                    if ($yearly_distribution > 0){
                                                        $tax_due = $tax_due + ($yearly_distribution * 0.37);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }else{
        if($yearly_distribution <= 9525){
             // 10%
            $tax_due = $yearly_distribution * 0.1;
        }else{
             // 10% of first $9,525 
            $tax_due = 952.5;
            $yearly_distribution -= 9525;
            if ($yearly_distribution > 0){
                if ($yearly_distribution <= 29175) {
                    $tax_due = $tax_due + ($yearly_distribution * 0.12); 
                }else{
                    // 12% of next $29175 ($38,700 - $9,525)
                    $tax_due = $tax_due + 3501; 
                    $yearly_distribution -= 29175;
                    if ($yearly_distribution > 0){
                        if ($yearly_distribution <= 43800) {
                            $tax_due = $tax_due + ($yearly_distribution * 0.22);
                        }else{
                            // 22% of next $43,800 ($82,500 - $29,175 - $9,525)
                            $tax_due = $tax_due + 19272; 
                            $yearly_distribution -= 43800;
                            if ($yearly_distribution > 0){
                                if ($yearly_distribution <= 75000) {
                                    $tax_due = $tax_due + ($yearly_distribution * 0.24);
                                }else{
                                    // 24% of next $75,000 ($157,500 - $43,800 - $29,175 - $9,525)
                                    $tax_due = $tax_due + 18000; 
                                    $yearly_distribution -= 75000;
                                    if ($yearly_distribution > 0){
                                        if ($yearly_distribution <= 42500) {
                                            $tax_due = $tax_due + ($yearly_distribution * 0.32);
                                        }else{
                                            // 32% of next $42,500 ($200,000 - $75,000 - $43,800 - $29,175 - $9,525)
                                            $tax_due = $tax_due + 13600; 
                                            $yearly_distribution -= 42500;
                                            if ($yearly_distribution > 0){
                                                if ($yearly_distribution <= 300000) {
                                                    $tax_due = $tax_due + ($yearly_distribution * 0.35);
                                                }else{
                                                    // 35% of next $300,000 ($500,000 - $42,500 - $75,000 - $43,800 - $29,175 - $9,525)
                                                    $tax_due = $tax_due + 105000; 
                                                    $yearly_distribution -= 300000;
                                                    if ($yearly_distribution > 0){
                                                        $tax_due = $tax_due + ($yearly_distribution * 0.37);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    return (double)$tax_due;
}


function monthly_ssa_inflation($ssa_amount, $years_in_retirement, $inflation_rate){
    $amount = $ssa_amount * 12 * $years_in_retirement;
    
    if($inflation_rate > 1.66){
        $total_after_inflation = adjust_for_inflation($amount, $years_in_retirement, ($inflation_rate - 1.66));
        return (double)(($total_after_inflation / $years_in_retirement) / 12);
    }else{
        if(($inflation_rate == 0) || ($inflation_rate == 1.66)){
            return $ssa_amount;
        }else{
            $total_after_deflation = interest($amount, 0, $years_in_retirement, (1.66 - $inflation_rate));
            return (double)(($total_after_deflation / $years_in_retirement) / 12);
        }
    }
}


/*
    Annuity = Yearly Distribution
    Matches: http://moneychimp.com/calculator/annuity_calculator.htm
    Formula From: http://moneychimp.com/articles/finworks/fmpayout.htm
    annuity = [ P(( 1 + r ) ^ (Y - 1)) * r ] / [ (( 1 + r ) ^ Y) - 1 ]
*/
function get_annuity($amount, $years_in_retirement, $interest_rate){
    if($interest_rate >= 0.5){
        $interest_rate_decimal = $interest_rate / 100;
    
        $left_power = pow(1 + $interest_rate_decimal, $years_in_retirement - 1);
        $left_return = ($amount * $left_power) * $interest_rate_decimal;
        $right_return = pow( 1 + $interest_rate_decimal, $years_in_retirement) - 1;    
        $return = ($left_return / $right_return);
        
        return (double)$return;
    }else{
        return (double)$amount/$years_in_retirement;
    }
}

?>


<div id="calc_content">
<h3>A REAL Retirement Calculator</h3>

<?php
$initial_pretax=0;
$initial_posttax=0;
$years_until=20;
$years_after=35;
$market_rate=7;
$interest_rate_after_retirement = 3;
$inflation_rate=2.5;
$contrib_pretax=100;
$contrib_posttax=100;
$target=0;
$fees_dollar_pretax=0;
$fees_dollar_posttax=0;
$fees_percent_pretax=0;
$fees_percent_posttax=0;
$ssa = 0;

if (isset($_POST['initial_pretax'])){$initial_pretax=$_POST['initial_pretax'];}
if (isset($_POST['initial_posttax'])){$initial_posttax=$_POST['initial_posttax'];}
if (isset($_POST['contrib_pretax'])){$contrib_pretax=$_POST['contrib_pretax'];}
if (isset($_POST['contrib_posttax'])){$contrib_posttax=$_POST['contrib_posttax'];}
if (isset($_POST['years_until'])){$years_until=$_POST['years_until'];}
if (isset($_POST['years_after'])){$years_after=$_POST['years_after'];}
if (isset($_POST['market_rate'])){$market_rate=$_POST['market_rate'];}
if (isset($_POST['interest_rate_after_retirement'])){$interest_rate_after_retirement=$_POST['interest_rate_after_retirement'];}
if (isset($_POST['inflation_rate'])){$inflation_rate=$_POST['inflation_rate'];}
if (isset($_POST['ssa'])){$ssa=$_POST['ssa'];}
if (isset($_POST['target'])){$target=$_POST['target'];}
if (isset($_POST['married'])){$married=true;}else{ $married=false; }

if (isset($_POST['fees_dollar_pretax'])){$fees_dollar_pretax=$_POST['fees_dollar_pretax'];}
if (isset($_POST['fees_dollar_posttax'])){$fees_dollar_posttax=$_POST['fees_dollar_posttax'];}
if (isset($_POST['fees_percent_pretax'])){$fees_percent_pretax=$_POST['fees_percent_pretax'];}
if (isset($_POST['fees_percent_posttax'])){$fees_percent_posttax=$_POST['fees_percent_posttax'];}

?>
<form method="POST" action="<?php echo getPage(); ?>#results">

<p>Before tax (401k/traditional) already in account: <?php echo '<input type="number" name="initial_pretax" value='. $initial_pretax.' required/>'?> </p>
<p>After tax (ROTH IRA/ROTH 401k) already in account: <?php echo '<input type="number" name="initial_posttax" value='. $initial_posttax.' required/>'?> </p>

<p>Before tax monthly contribution (include your employer's match): <?php echo '<input type="number" name="contrib_pretax" value='. $contrib_pretax.' step="0.01" min="0" required/>'?> </p>
<p>After tax monthly contribution (include your employer's match): <?php echo '<input type="number" name="contrib_posttax" value='. $contrib_posttax.' step="0.01" min="0" required/>'?> </p>


<p>Before tax $/year fees: <?php echo '<input type="number" name="fees_dollar_pretax" value='. $fees_dollar_pretax.' step="0.01" min="0" required/>'?> &nbsp; %/year fees: <?php echo '<input type="number" name="fees_percent_pretax" value='. $fees_percent_pretax.' step="0.01" min="0" required/>'?> </p>
<p>After tax $/year fees: <?php echo '<input type="number" name="fees_dollar_posttax" value='. $fees_dollar_posttax.' step="0.01" min="0" required/>'?> &nbsp; %/year fees: <?php echo '<input type="number" name="fees_percent_posttax" value='. $fees_percent_posttax.' step="0.01" min="0" required/>'?> </p>

<hr style="width: 60%;">

<p>Years until Retirement*: <?php echo '<input type="number" name="years_until" value='. $years_until.' min="1" required/>' ?></p>

<p>Estimated Years in Retirement: <?php echo '<input type="number" name="years_after" value='. $years_after.' min="1" required/>' ?></p>

<p><input type="checkbox" name="married" checked> Married through retirement? (this changes your taxes)</p>

<hr style="width: 60%;">

<p>Estimated Yearly Market Gain/Return: <?php echo '<input type="number" name="market_rate" value='.$market_rate.' />' ?> % </p>
<p>Interest Rate After Retirement: <?php echo '<input type="number" name="interest_rate_after_retirement" value='.$interest_rate_after_retirement.'  step="0.5" min="0" max="10" />' ?> % </p>
<p>Average Inflation Rate: <?php echo '<input type="number" name="inflation_rate" value='.$inflation_rate.' step="0.5" min="0" max="10" />'?> % </p>

<hr style="width: 60%;">

<p>Social Security Monthly Estimate (<a href="https://www.ssa.gov/benefits/retirement/estimator.html" target="_blank">Offical SSA Estimate</a> or <a href="https://smartasset.com/retirement/social-security-calculator" target="_blank">Unofficial Estimate</a>):  $ <?php echo '<input type="number" name="ssa" value='. $ssa.' required/>'?> </p> 

<p> <input type="submit" name="submit" value="Calculate"/> </p>
</form>

<br>

<?php
// IF form was submitted
if (isset($_POST['submit'])){
    setlocale(LC_MONETARY, 'en_US.UTF-8');
    $pretax_int = interest($initial_pretax,$contrib_pretax,$years_until,$market_rate);
    $posttax_int = interest($initial_posttax,$contrib_posttax,$years_until,$market_rate);
    
    $pretax_int_after = get_annuity($pretax_int, $years_after, $interest_rate_after_retirement);
    $pretax_int_after_2 = get_annuity($pretax_int, $years_after, 0);
    $pretax_int_after_total = $pretax_int_after - $pretax_int_after_2;
    
    $posttax_int_after = get_annuity($posttax_int, $years_after, $interest_rate_after_retirement);
    $posttax_int_after_2 = get_annuity($posttax_int, $years_after, 0);
    $posttax_int_after_total = $posttax_int_after - $posttax_int_after_2;
    
    $pretax_total = $pretax_int + $pretax_int_after_total;
    $posttax_total = $posttax_int + $posttax_int_after_total;
    
    
    
    $pretax_int_fees = interest($initial_pretax,$contrib_pretax,$years_until,($market_rate-$fees_percent_pretax)) - ($fees_dollar_pretax*$years_until);
    $pretax_int_after_fees = get_annuity($pretax_int_fees, $years_after, $interest_rate_after_retirement);
    $pretax_int_after_2_fees = get_annuity($pretax_int_fees, $years_after, 0);
    $pretax_int_after_total_fees = $pretax_int_after_fees - $pretax_int_after_2_fees;
    
    $posttax_int_fees = interest($initial_posttax,$contrib_posttax,$years_until,($market_rate-$fees_percent_posttax)) - ($fees_dollar_posttax*$years_until);
    $posttax_int_after_fees = get_annuity($posttax_int_fees, $years_after, $interest_rate_after_retirement);
    $posttax_int_after_2_fees = get_annuity($posttax_int_fees, $years_after, 0);
    $posttax_int_after_total_fees = $posttax_int_after_fees - $posttax_int_after_2_fees;
    
    
    $pretax_contrib_total = $initial_pretax + ($contrib_pretax * 12 * $years_until);
    $posttax_contrib_total = $initial_posttax + ($contrib_posttax * 12 * $years_until);
    
    $pretax_int_total = ($pretax_int + ($pretax_int_after_total_fees * $years_after)) - $pretax_contrib_total;
    $posttax_int_total = ($posttax_int + ($posttax_int_after_total_fees * $years_after)) - $posttax_contrib_total;
    
    
    
    $pretax_fees = get_fees($initial_pretax,$contrib_pretax,$years_until, $years_after,$market_rate, $interest_rate_after_retirement, $fees_percent_pretax, $fees_dollar_pretax);
    $posttax_fees = get_fees($initial_posttax,$contrib_posttax,$years_until, $years_after, $market_rate, $interest_rate_after_retirement, $fees_percent_posttax, $fees_dollar_posttax);
    
    ////////////////////////////////
    
    
    $pretax_annuity_minus_fees = get_annuity($pretax_int_fees, $years_after, ($interest_rate_after_retirement - $fees_percent_pretax)) - $fees_dollar_pretax;
    
    $pretax_annual_taxes = get_annual_taxes($pretax_annuity_minus_fees, $married);
    $pretax_total_taxes = $pretax_annual_taxes * $years_after;
    
    //////////////////////////////// copy above, below for posttax for annuity calc
    
    $posttax_annuity_minus_fees = get_annuity($posttax_int_fees, $years_after, ($interest_rate_after_retirement - $fees_percent_posttax)) - $fees_dollar_posttax;
    
    ////////////////////////////////
    
    
    // (annuity ( interest minus % fees ) - (flat_fees * (years_until + next year))) - taxes
    $pretax_annuity = ($pretax_annuity_minus_fees/12) - ($pretax_annual_taxes/12);
    
    
    // annuity ( interest minus % fees ) - (flat_fees * (years_until + next year))
    $posttax_annuity = $posttax_annuity_minus_fees/12;
    
    $pretax_total_account_value = ($pretax_contrib_total + $pretax_int_total) - ($pretax_fees + $pretax_total_taxes);
    $posttax_total_account_value = $posttax_contrib_total + $posttax_int_total - $posttax_fees;
    
    if($inflation_rate > 0){
        // AFTER TAXES!
        $pretax_inflation = adjust_for_inflation($pretax_annuity, $years_until, $inflation_rate);
        $posttax_inflation = adjust_for_inflation($posttax_annuity, $years_until, $inflation_rate);
    }else{
        $pretax_inflation = 'None set.';
        $posttax_inflation = 'None set.';
    }
?>
    <br><br><hr style="width: 80%;"><br><br>
    
    <h3 id="results">Results:</h3>
    <p>After <?php echo $years_until; ?> years, your account's values are:</p>

    <h4>Total Value:</h4>
    <div class="table_div">
    <table>
    <tr>
        <td>Account</td>
        <td>Contribution</td>
        <td>Interest</td>
        <td>Total Fees</td>
        <td>Total Taxes***</td>
        <td>&nbsp;</td>
        <td>Total Account Value</td>
    </tr>
    <tr>
        <td>Before Tax (401k/traditional)</td>
        <td><?php echo money_format('%.2n', $pretax_contrib_total); ?></td>
        <td><?php echo money_format('%.2n', $pretax_int_total); ?></td>
        <td><?php echo money_format('%.2n', $pretax_fees); ?></td>
        <td><?php echo money_format('%.2n', $pretax_total_taxes); ?></td>
        <td>&nbsp;</td>
        <td><?php echo money_format('%.2n', $pretax_total_account_value); ?></td>
    </tr>
    <tr>
        <td>After Tax (ROTH IRA/ROTH 401k)</td>
        <td><?php echo money_format('%.2n', $posttax_contrib_total); ?></td>
        <td><?php echo money_format('%.2n', $posttax_int_total); ?></td>
        <td><?php echo money_format('%.2n', $posttax_fees); ?></td>
        <td>-Not Taxed-</td>
        <td>&nbsp;</td>
        <td><?php echo money_format('%.2n', $posttax_total_account_value); ?></td>
    </tr>
    <tr>
        <td>Total</td>
        <td><?php echo money_format('%.2n', $pretax_contrib_total + $posttax_contrib_total); ?></td>
        <td><?php echo money_format('%.2n', $pretax_int_total + $posttax_int_total); ?></td>
        <td><?php echo money_format('%.2n', $pretax_fees + $posttax_fees); ?></td>
        <td><?php echo money_format('%.2n', $pretax_total_taxes); ?></td>
        <td>&nbsp;</td>
        <td><?php echo money_format('%.2n', $pretax_total_account_value + $posttax_total_account_value); ?></td>
    </tr>
    </table>
    </div>
    
    <br><hr style="width: 60%;">
    See the difference compounding interest makes! <a href="https://www.investor.gov/additional-resources/free-financial-planning-tools/compound-interest-calculator" target="_blank">SEC Compound Interest Calculator</a>
    <br><hr style="width: 60%;"><br>
    
    <h4>Yearly Amounts:</h4>
    <div class="table_div">
    <table>
    <tr>
        <td>Account</td>
        <td>Yearly Taxes Due</td>
        <td>Yearly Retirement Payout</td>
<?php if($inflation_rate > 0){ ?><td>Yearly Retirement Payout<br>Adjusted for Inflation in Today's Dollars</td><?php } ?>
    </tr>
    <tr>
        <td>Before Tax (401k/traditional)</td>
        <td><?php echo money_format('%.2n', $pretax_annual_taxes); ?></td>
        <td><?php echo money_format('%.2n', $pretax_annuity*12); ?></td>
        <?php if($inflation_rate > 0){ ?><td><?php echo money_format('%.2n', $pretax_inflation*12); ?></td><?php } ?>
    </tr>
    <tr>
        <td>After Tax (ROTH IRA/ROTH 401k)</td>
        <td>-No Taxes-</td>
        <td><?php echo money_format('%.2n', $posttax_annuity*12); ?></td>
        <?php if($inflation_rate > 0){ ?><td><?php echo money_format('%.2n', $posttax_inflation*12); ?></td><?php } ?>
    </tr>
    <tr>
        <td>Total</td>
        <td><?php echo money_format('%.2n', $pretax_annual_taxes); ?></td>
        <td><?php echo money_format('%.2n', ($pretax_annuity*12) + ($posttax_annuity*12)); ?></td>
        <?php if($inflation_rate > 0){ ?><td><?php echo money_format('%.2n', ($pretax_inflation*12) + ($posttax_inflation*12)); ?></td><?php } ?>
    </tr>
    </table>
    </div>
    
<?php if($inflation_rate > 0){ ?>
    <br><hr style="width: 60%;">
    Yes, the inflation numbers are correct! (I know it's a huge difference.)<br>Not planning for inflation can really mess up a retirement plan.<br>See inflation rates for other years here: <a href="https://smartasset.com/investing/inflation-calculator" target="_blank">Inflation Calculator</a> <br>
    <a href="https://www.vertex42.com/Calculators/inflation-calculator.html" target="_blank">Another Inflation Calculator</a>
<?php } ?>
    <br><hr style="width: 60%;"><br>
    
    <h4>Monthly Totals:</h4>
    <div class="table_div">
    <table>
    <tr>
        <td>&nbsp;</td>
        <td>Before Tax Accounts</td>
        <td>After Tax Accounts</td>
        <td>Social Security</td>
        <td>Total per Month</td>
    </tr>
    <tr>
        <td>No Inflation</td>
        <td><?php echo money_format('%.2n', $pretax_annuity); ?></td>
        <td><?php echo money_format('%.2n', $posttax_annuity); ?></td>
        <td><?php echo money_format('%.2n', $ssa); ?></td>
        <td><?php echo money_format('%.2n', $pretax_annuity + $posttax_annuity + $ssa); ?></td>
    </tr>
    <tr>
        <?php if($inflation_rate > 0){ ?>
        <td>Inflation Rate of <?php echo $inflation_rate; ?>%</td>
        <td><?php echo money_format('%.2n', $pretax_inflation); ?></td>
        <td><?php echo money_format('%.2n', $posttax_inflation); ?></td>
        <td><?php echo money_format('%.2n', monthly_ssa_inflation($ssa, $years_after, $inflation_rate)); ?>**</td>
        <td><?php echo money_format('%.2n', $pretax_inflation + $posttax_inflation + monthly_ssa_inflation($ssa, $years_after, $inflation_rate)); ?></td>
        <?php } ?>
    </tr>
    </table>
    </div>
    
    <?php if($inflation_rate == 0){ ?>
    <p>However, inflation WILL eat a huge chunk out of this! To see what that amount will be worth in today's dollars, set the inflation rate above. 2.5% is the annual average for the United States.</p>
    <?php }else{ if($inflation_rate < 2.5){ ?>
    <p>However, the average inflation rate for the United States is 2.5%! You should be using that number or a higher one!</p>
    <?php }}
}
?>

    <br>&nbsp;<br><hr style="width: 60%;"><br>
    <p>*This calculator assumes you retire after you reach 59 1/2 years old. Retiring before this age will cause you to incur additional fees and taxes!</p>
    <p>**Social Security does adjust for inflation somewhat. However, their adjustments are often far below the actual inflation rate of that year. In the last 10 years, the Social Security Administration has adjusted for an average inflation rate of 1.66%. (<a href="https://www.ssa.gov/news/cola/" target="_blank">Source</a>)</p>
    <p>***Taxes are based on the 2018 tax rates.</p>

<br><br>&nbsp;<br><br>

</div>

<br><br>&nbsp;<br><br>

</body> </html> 
